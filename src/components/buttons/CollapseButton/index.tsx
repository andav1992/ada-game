import React, { useState } from 'react';
import SmoothCollapse from 'react-smooth-collapse';
import { Button, IProps } from '../Button';

// #region -------------- Component -------------------------------------------------------------------

export const CollapseButton: React.FunctionComponent<IProps> = ({ text, icon, children }) => {
  const [isExpanded, setIsExpanded] = useState<boolean>(false);

  return (
    <div>
      <SmoothCollapse expanded={isExpanded}>{children}</SmoothCollapse>

      {!isExpanded && <Button text={text} icon={icon} onClick={() => setIsExpanded(!isExpanded)} />}
    </div>
  );
};

// #endregion
