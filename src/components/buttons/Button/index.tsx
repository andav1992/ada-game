import React, { ButtonHTMLAttributes } from 'react';
import styled from 'styled-components';

// #region -------------- Interfaces -------------------------------------------------------------------

export interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: React.ReactNode;
  text?: string;
}

// #endregion

// #region -------------- Component -------------------------------------------------------------------

export const Button: React.FunctionComponent<IProps> = ({ icon, text, ...otherProps }) => (
  <StyledButton type="button" {...otherProps}>
    <span>
      {icon}
      {text}
    </span>
  </StyledButton>
);

// #endregion

// #region -------------- Style -------------------------------------------------------------------

const StyledButton = styled.button`
  position: relative;
  display: inline-block;
  height: 46px;
  padding: 10px 22px;
  font-size: 14px;
  font-weight: bold;
  background-color: #05668d;
  color: white;
  border: none;
  border-radius: 25px;
  box-sizing: border-box;
  box-shadow: none;
  text-transform: uppercase;
  outline: none;
  cursor: pointer;

  @media (max-width: 575px) {
    width: 100%;
  }

  &.btn-green {
    background-color: #02a896;
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(255, 255, 255, 0);
    box-sizing: border-box;
    border-radius: 25px;
    transition: background-color 0.3s;
  }

  @media (min-width: 768px) {
    &:hover:not(:disabled) {
      color: white;

      &::after {
        background-color: rgba(255, 255, 255, 0.4);
      }
    }
  }

  &:focus {
    box-shadow: none;
  }

  &:disabled {
    opacity: 0.3;
    cursor: not-allowed;
  }

  > span {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  svg {
    width: 22px;
    height: 22px;
    margin-right: 10px;
  }

  + button {
    margin-left: 20px;

    @media (max-width: 575px) {
      margin-top: 20px;
      margin-left: 0;
    }
  }
`;

// #endregion
