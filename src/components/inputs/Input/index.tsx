import React, { InputHTMLAttributes } from 'react';
import styled from 'styled-components';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IProps extends InputHTMLAttributes<HTMLInputElement> {}

// #endregion

// #region -------------- Component -------------------------------------------------------------------

export const Input: React.FunctionComponent<IProps> = (props) => <StyledInput {...props} />;

// #endregion

// #region -------------- Style -------------------------------------------------------------------

const StyledInput = styled.input`
  width: 100%;
  height: 40px;
  padding: 10px;
  font-size: 15px;
  color: black;
  background-color: transparent;
  border-style: none;
  border: solid 1px rgba(2, 168, 150, 0.4);
  border-radius: 0;
  outline: 0;
  transition: border-color 0.5s;

  &:focus {
    border-color: #02a896;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

// #endregion
