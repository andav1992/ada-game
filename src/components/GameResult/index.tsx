import React from 'react';
import { IImage } from 'src/models/image';
import styled from 'styled-components';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IProps {
  image: IImage;
  text: string;
}

// #endregion

// #region -------------- Component -------------------------------------------------------------------

export const GameResult: React.FunctionComponent<IProps> = ({ image, text }) => (
  <StyledGameResult>
    <img src={image.src} alt={image.alt} />
    <p>{text}</p>
  </StyledGameResult>
);

// #endregion

// #region -------------- Style -------------------------------------------------------------------

const StyledGameResult = styled.div`
  img {
    max-width: 240px;
    width: 100%;
  }

  p {
    margin: 30px 0;
    font-size: 24px;
    font-weight: bold;
  }
`;

// #endregion
