import { checkIfGameWinnable } from '../index';

test('winnable game', () => {
  expect(checkIfGameWinnable([1, 2, 0, 3, 0, 2, 0])).toBe<boolean>(true);
  expect(checkIfGameWinnable([1, 2, 1, 0])).toBe<boolean>(true);
});

test('not winnable game', () => {
  expect(checkIfGameWinnable([1, 2, 0, 1, 0, 2, 0])).toBe<boolean>(false);
  expect(checkIfGameWinnable([2, 1, 0, 2])).toBe<boolean>(false);
});
