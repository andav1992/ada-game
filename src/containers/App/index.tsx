import React, { useState } from 'react';
import { ReactComponent as AddIcon } from 'src/assets/images/add-icon.svg';
import { ReactComponent as GameIcon } from 'src/assets/images/game-icon.svg';
import { ReactComponent as PlayIcon } from 'src/assets/images/play-icon.svg';
import { ReactComponent as RemoveIcon } from 'src/assets/images/remove-icon.svg';
import { ReactComponent as RestartIcon } from 'src/assets/images/restart-icon.svg';
import { Button } from 'src/components/buttons/Button';
import { CollapseButton } from 'src/components/buttons/CollapseButton';
import { Input } from 'src/components/inputs/Input';
import { GameResult } from 'src/components/GameResult';
import styled from 'styled-components';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IStep {
  value: string;
  error?: string;
}

// #endregion

// #region -------------- App -------------------------------------------------------------------

export const App: React.FunctionComponent<{}> = () => {
  const [steps, setSteps] = useState<IStep[]>([{ value: '0' }, { value: '0' }, { value: '0' }, { value: '0' }]);
  const [isPlayed, setIsPlayed] = useState<boolean>(false);
  const [isWinnable, setIsWinnable] = useState<boolean>(false);
  const isNewStepAdded = steps.length > 4;

  return (
    <Container>
      <Logo>
        <img src={require('src/assets/images/logo.png')} alt="aDa Game" />
      </Logo>
      <Description>
        <b>Game rules:</b>
        <List>
          <li>Game requires at least four steps — you can add more steps</li>
          <li>Step value can be between 0 and 3</li>
          <li>You start at first step</li>
          <li>
            Current step value determines how much steps you can take at maximum, e.g. if the value is 3 you can take 0,
            1, 2 or 3 steps
          </li>
          <li>Game goal is to reach last step</li>
        </List>
      </Description>
      <CollapseButton text="Start game" icon={<GameIcon />}>
        {!isPlayed ? (
          <>
            <Steps>
              {steps &&
                steps.map((step, index) => (
                  <Step key={index}>
                    <Label>Step {index + 1}</Label>
                    <StepInput>
                      <div>
                        <Input
                          type="text"
                          value={step.value}
                          maxLength={1}
                          autoFocus={isNewStepAdded ? index === steps.length - 1 : index === 0}
                          onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChange(index, e.target.value)}
                        />
                      </div>
                      {isNewStepAdded && (
                        <Button
                          className="btn-green"
                          title="Remove step"
                          icon={<RemoveIcon />}
                          onClick={() => onRemoveStep(index)}
                        />
                      )}
                    </StepInput>
                    {step.error && <StepError>{step.error}</StepError>}
                  </Step>
                ))}
            </Steps>
            <Button text="Add step" icon={<AddIcon />} onClick={onAddStep} />
            <Button className="btn-green" text="Play game" icon={<PlayIcon />} onClick={onPlayGame} />
          </>
        ) : (
          <Result>
            {isWinnable ? (
              <GameResult image={{ src: require('src/assets/images/victory.png'), alt: 'Victory' }} text="You win!" />
            ) : (
              <GameResult image={{ src: require('src/assets/images/loss.png'), alt: 'Loss' }} text="You lose!" />
            )}
            <Button text={isWinnable ? 'Restart game' : 'Try again'} icon={<RestartIcon />} onClick={onRestartGame} />
          </Result>
        )}
      </CollapseButton>
    </Container>
  );

  function onChange(index: number, value: string) {
    const newSteps = [...steps];
    const newStep = newSteps[index];

    if (newStep) {
      newStep.value = isNaN(Number(value)) ? '' : value;
      setSteps(newSteps);
    }
  }

  function onAddStep() {
    const newStep: IStep = { value: '0' };
    setSteps([...steps, newStep]);
  }

  function onRemoveStep(index: number) {
    const newSteps = [...steps];
    const newStep = newSteps[index];

    if (newStep) {
      newSteps.splice(index, 1);
      setSteps(newSteps);
    }
  }

  function onPlayGame() {
    const newSteps = [...steps];
    let isError = false;

    newSteps.forEach((step) => {
      if (!step.value) {
        step.error = 'Value must be filled';
        isError = true;
      } else {
        const value = Number(step.value);

        if (isNaN(value)) {
          step.error = 'Value must be a number';
          isError = true;
        } else if (value < 0 || value > 3) {
          step.error = 'Value must be between 0 and 3';
          isError = true;
        }
      }
    });

    if (isError) {
      setSteps(newSteps);
      return;
    }

    const values = newSteps.map((step) => Number(step.value));
    const isWinnable = checkIfGameWinnable(values);

    setIsPlayed(true);
    setIsWinnable(isWinnable);
  }

  function onRestartGame() {
    setSteps([{ value: '0' }, { value: '0' }, { value: '0' }, { value: '0' }]);
    setIsPlayed(false);
    setIsWinnable(false);
  }
};

// #endregion

// #region -------------- Testable functions -------------------------------------------------------------------

export function checkIfGameWinnable(steps: number[], from: number = 0, to: number = 1): boolean {
  if (steps.length <= to) {
    return true;
  }

  for (let i = from; i < to; i++) {
    const step = steps[i];

    if (step === 0) {
      continue;
    }

    const newFrom = i + 1;
    const newTo = newFrom + step;

    if (checkIfGameWinnable(steps, newFrom, newTo)) {
      return true;
    }
  }

  return false;
}

// #endregion

// #region -------------- Styles -------------------------------------------------------------------

const Container = styled.div`
  max-width: 800px;
  padding: 0 15px;
  margin: 0 auto;
`;

const Logo = styled.div`
  padding: 20px 0;
  text-align: center;

  img {
    width: 100%;
    max-width: 150px;
  }
`;

const Description = styled.div`
  margin: 20px 0;
  color: #8d97ab;
`;

const List = styled.ol`
  margin: 0;
  padding: 16px;
`;

const Steps = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -10px 10px;
  text-align: left;
`;

const Step = styled.div`
  width: 20%;
  margin-bottom: 20px;
  padding: 0 10px;

  @media (max-width: 575px) {
    width: 50%;
  }

  @media (min-width: 576px) and (max-width: 767px) {
    width: 25%;
  }
`;

const Label = styled.div`
  margin-bottom: 10px;
  line-height: 16px;
  font-size: 12px;
  font-weight: bold;
`;

const StepInput = styled.div`
  display: flex;

  button {
    height: 40px;
    width: 40px;
    min-width: 40px;
    padding: 0;
    border-radius: 0;

    &::after {
      border-radius: 0;
    }

    > span {
      justify-content: center;
    }

    svg {
      margin: 0;
    }
  }
`;

const StepError = styled.div`
  margin-top: 10px;
  padding: 7px 10px;
  line-height: 16px;
  font-size: 12px;
  color: #ca0000;
  background-color: #fff0f0;
`;

const Result = styled.div`
  text-align: center;
`;

// #endregion
