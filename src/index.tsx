import 'core-js';
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from 'src/containers/App';

// Styles
import 'sanitize.css';
import 'src/style/index.scss';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
